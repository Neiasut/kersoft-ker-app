<?php

namespace controllers;

use Ker;
use ker\base\Addictions;
use ker\base\Asset;
use ker\base\Controller;
use models\main\Index;

class MainController extends Controller
{
    public function __construct()
    {
        parent::__construct('pages/main');
    }

    public function actionIndex()
    {
        Asset::includeAssets([
            'Test',
            'AllPage',
            'Test2'
        ]);

        $model = new Index();
        $data = $model->getData($this);

        Ker::$app->twig->setGlobalVariable('title', 'Some site');

        return $this->renderPage(
            [
                'header' => 'header',
                'content' => [
                    'path' => $this->getRenderPath() . '/actions/index.html.twig',
                    'variables' => $data
                ],
                'footer' => 'footer'
            ]
        );
    }

    public function actionTemplate()
    {
        return Controller::renderTemplate('pages/main/actions/index.html.twig');
    }
}
