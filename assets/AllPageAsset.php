<?php

namespace assets;

use Ker;
use ker\base\Addictions;
use ker\base\Asset;

class AllPageAsset extends Asset
{
    public function addResources()
    {
        Ker::$app->resources->addInlineJS('
            <script>
                console.log("test2");
            </script>
        ', true);
        $this->addJS('main.js', new Addictions());
    }

    public function addAddictions()
    {
        return new Addictions();
    }
}
