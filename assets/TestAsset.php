<?php

namespace assets;

use ker\base\Asset;
use ker\base\Addictions;

class TestAsset extends Asset
{
    public function addResources()
    {
        //Ker::$app->resources->addJSFile('/test.js', new Addictions());
        $this->addJS('/test.js', new Addictions());
    }

    public function addAddictions()
    {
        return new Addictions(['AllPage']);
    }
}
