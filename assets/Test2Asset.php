<?php

namespace assets;

use ker\base\Asset;
use ker\base\Addictions;

class Test2Asset extends Asset
{
    public function addResources()
    {
        $this->addJS('/test2.js', new Addictions());
        //$this->addJS('/test.js', new Addictions());
    }

    public function addAddictions()
    {
        return new Addictions(['AllPage', 'Test']);
    }
}