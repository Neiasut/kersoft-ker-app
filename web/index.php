<?php

require(__DIR__ . '/../vendor/kersoft/ker/Ker.php');

Ker::setAlias('@ROOT', dirname(__DIR__));

$config = require_once(__DIR__ . '/../config/config.php');

(new \ker\web\Application($config))->run();
