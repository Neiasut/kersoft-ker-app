<?php

$config = [
    'controllersPath' => '/controllers',
    'publicPath' => '/web',
    'useShortNames' => true,
    'prettyUrlControllers' => [
        'main' => ''
    ],
    'typeUsersForAllContent' => ['anonymous', 'user', 'redactor', 'admin'],
    'modelsPath' => '/models',
    'viewsPath' => '/views',
    'assetsPath' => '/assets',
    'usersInfoPath' => '/BD/users',
    'systemPage' => [
        '404' => '404',
        '403' => '403'
    ]
];

return $config;
